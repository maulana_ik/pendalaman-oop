<?php 
trait Hewan {
	public $nama,
		   $darah = 50,
		   $jumlahKaki,
		   $keahlian;

	public function atraksi(){
		return "$this->nama sedang $this->keahlian";
	}
}

trait Fight {
	public $attackPower,
		   $defencePower;
	public function serang($target){
		$dummy = $target->nama;
		return "$this->nama sedang menyerang $dummy";
	}

	public function diserang($target){
		$dummy = $target->nama;
		$ap = $this->attackPower;
		$dp = $target->defencePower;
		$sisaDarah = 50 - ($ap/$dp);
		return "$dummy sedang diserang $this->nama, sisa darah $sisaDarah";
	}
}

class Elang{
	
	public function getIntoHewan(){
		$this->nama = "Elang";
		$this->jumlahKaki = 2;
		$this->keahlian = "Terbang tinggi";
		$this->attackPower = 10;
		$this->defencePower = 5;
		
		return "$this->nama , darah: $this->darah, jumlahKaki: $this->jumlahKaki, $this->keahlian,
		attackPower: $this->attackPower, defencePower: $this->defencePower";
	}
	use Hewan, Fight;
}

class Harimau{
	use Hewan, Fight;
	public function getIntoHewan(){
		$this->nama = "Harimau";
		$this->jumlahKaki = 4;
		$this->keahlian = "Lari cepat";
		$this->attackPower = 7;
		$this->defencePower = 8;
		
		return "$this->nama , darah: $this->darah, jumlahKaki: $this->jumlahKaki, $this->keahlian,
		attackPower: $this->attackPower, defencePower: $this->defencePower";
	}
}

$elang = new Elang();
echo $elang->getIntoHewan() . "<br>";
echo "ATRAKSI" . "<br>";
echo $elang->atraksi() . "<br>" . "<br>";


$harimau = new Harimau();
echo $harimau->getIntoHewan() . "<br>";
echo "ATRAKSI" . "<br>";
echo $harimau->atraksi();
echo "<br>";echo "<br>";


echo "OUTPUT FIGHT";
echo "<br>";
echo $elang->serang($harimau);
echo "<br>";
echo $elang->diserang($harimau);
echo "<br>";
echo $harimau->serang($elang);
echo "<br>";
echo $harimau->diserang($elang);
?>